# GeekHub X

Java for Web project for GeekHub, Season X

##########################################

|Lesson|Date|Topic|
|:----:|:--:|:----|
| 1|October 19, 2020|Intro|
| 2|October 26, 2020|Basics|
| 3|November 2, 2020|Object-oriented Programming|
| 4|November 9, 2020|Error Propagation and Handling|
| 5|November 16, 2020|Code Testing (Unit)|
| 6|November 23, 2020|Practice|
| 7|November 30, 2020|Generics. Collections Framework|
| 8|December 7, 2020|Gradle, DateTime API, Optional Class|
| 9|December 14, 2020|Functional Programming, Stream API|
|10|December 21, 2020|Reflection API|
|11|December 28, 2020|Input/Output|
|12|January 18, 2021|Servlet API (Web)|
|13|January 25, 2021|JDBC|
|14|February 1, 2021|Spring IoC|
|15|February 8, 2021|Spring JDBC, Flyway|
|16|February 15, 2021|Spring MVC, Boot|
|17|February 22, 2021|Practice|
|18|March 01, 2021|REST, Swagger|
|19|March 09, 2021|Spring Security p1.|
|20|March 15, 2021|Spring Security p2|
|21|March 22, 2021|Concurrency|
|22|March 29, 2021|Integration Testing|
|23|April 5, 2021|Course works preview|
|24|April 12, 2021|Hibernate, Spring Data|
|25|April 19, 2021|CI/CD|
|26|April 26, 2021|Course works review|
